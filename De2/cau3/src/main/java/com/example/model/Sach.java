package com.example.model;

public class Sach {
    private Integer maSach;
    private String tenSach;
    private String danhMuc;
    private String giaSach;

    public Sach() {
    }

    public Sach(String tenSach, String danhMuc, String giaSach) {
        this.tenSach = tenSach;
        this.danhMuc = danhMuc;
        this.giaSach = giaSach;
    }

    public Sach(Integer maSach, String tenSach, String danhMuc, String giaSach) {
        this.maSach = maSach;
        this.tenSach = tenSach;
        this.danhMuc = danhMuc;
        this.giaSach = giaSach;
    }

    public Integer getMaSach() {
        return maSach;
    }

    public void setMaSach(Integer maSach) {
        this.maSach = maSach;
    }

    public String getTenSach() {
        return tenSach;
    }

    public void setTenSach(String tenSach) {
        this.tenSach = tenSach;
    }

    public String getDanhMuc() {
        return danhMuc;
    }

    public void setDanhMuc(String danhMuc) {
        this.danhMuc = danhMuc;
    }

    public String getGiaSach() {
        return giaSach;
    }

    public void setGiaSach(String giaSach) {
        this.giaSach = giaSach;
    }
}