create database qlsach;

create table qlsach.sach
(
    ma_sach       int         not null auto_increment,
    ten_sach varchar(50) not null,
    danh_muc varchar(50) not null,
    gia_sach  varchar(50) not null,
    primary key (ma_sach)
);