package com.example.controller;

import com.example.model.Sach;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class SachServle extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private SachService sachService;
    public void init(){
        sachService = new SachService();
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rqParam = req.getServletPath();
        switch (rqParam){
            case "/insert":
                showFormThemSach(req, resp);
                break;
            case "/add":
                themSach(req,resp);
                break;
            case "/delete":
                xoaSach(req, resp);
                break;
            case "/edit":
                showFormUpdateSach(req, resp);
                break;
            case "/update":
                capNhatSach(req, resp);
                break;
            default:
                showListSach(req,resp);
                break;
        }
    }
    private void showListSach(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Sach> sachList = sachService.danhsachSach();
        req.setAttribute("listSach" , sachList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/list.jsp");
        dispatcher.forward(req,resp);
    }
    private void showFormThemSach(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/them.jsp");
        requestDispatcher.forward(req,resp);
    }
    private void themSach(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tenSach =req.getParameter("tenSach");
        String danhMuc = req.getParameter("danhMuc");
        String giaSach = req.getParameter("giaSach");
        Sach Sach = new Sach(tenSach, danhMuc,giaSach);
        sachService.themSach(Sach);
        resp.sendRedirect("list");
    }
    private void xoaSach(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maSach"));
        sachService.xoaSach(id);
        resp.sendRedirect("list");
    }
    private void showFormUpdateSach(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("maSach"));
        Sach Sach = sachService.layIDSach(id);
        if (Sach != null){
            req.setAttribute("maSach",id);
            req.setAttribute("sach", Sach);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/sua.jsp");
            requestDispatcher.forward(req,resp);
        }
    }
    private void capNhatSach(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maSach"));
        String tenSach =req.getParameter("tenSach");
        String danhMuc = req.getParameter("danhMuc");
        String giaSach = req.getParameter("giaSach");
        Sach Sach = new Sach(id,tenSach, danhMuc,giaSach);
        sachService.capnhatSach(Sach);
        resp.sendRedirect("list");
    }
}
