package com.example.controller;

import com.example.connection.SachDB;
import com.example.model.Sach;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SachService {
    static Connection connection;
    private static final String SELECT_ALL_FROM_Sach = "select * from sach;";
    private static final String INSERT_Sach = "insert into sach(ten_sach,danh_muc,gia_sach) values (?,?,?);";
    private static final String DELETE_Sach = "delete from sach where ma_sach = ?;";
    private static final String UPDATE_Sach = "update sach set ten_sach = ?, danh_muc = ?, gia_sach = ? where ma_sach = ?;";
    private static final String SELECTION_Sach_BY_IDSach = "select * from sach where ma_sach = ?;";

    public Boolean themSach(Sach sach) {
        try {
            connection = SachDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_Sach);
            preparedStatement.setString(1, sach.getTenSach());
            preparedStatement.setString(2, sach.getDanhMuc());
            preparedStatement.setString(3, sach.getGiaSach());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Sach> danhsachSach() {
        List<Sach> sachList = new ArrayList<>();
        try {
            connection = SachDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM_Sach);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Sach sach = new Sach();
                sach.setMaSach(resultSet.getInt("ma_sach"));
                sach.setTenSach(resultSet.getString("ten_sach"));
                sach.setDanhMuc(resultSet.getString("danh_muc"));
                sach.setGiaSach(resultSet.getString("gia_sach"));
                sachList.add(sach);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sachList;
    }

    public Boolean xoaSach(Integer id) {
        try {
            connection = SachDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_Sach);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Sach layIDSach(Integer id) {
        Sach sach = new Sach();
        try {
            connection = SachDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECTION_Sach_BY_IDSach);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                sach.setMaSach(resultSet.getInt("ma_sach"));
                sach.setTenSach(resultSet.getString("ten_sach"));
                sach.setDanhMuc(resultSet.getString("danh_muc"));
                sach.setGiaSach(resultSet.getString("gia_sach"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sach;
    }

    public void capnhatSach(Sach sach) {
        try {
            connection = SachDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_Sach);
            preparedStatement.setString(1, sach.getTenSach());
            preparedStatement.setString(2, sach.getDanhMuc());
            preparedStatement.setString(3, sach.getGiaSach());
            preparedStatement.setInt(4, sach.getMaSach());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

