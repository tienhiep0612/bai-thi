<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>LIST SACH</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
        }
        h1 {
            color: #333;
            text-align: center;
            padding: 20px 0;
        }
        table {
            width: 100%;
            margin: 20px 0;
            border-collapse: collapse;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ddd;
            text-align: left;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        a {
            color: #333;
            text-decoration: none;
            margin: 0 10px;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 5px;
            text-align: center;
            cursor: pointer;
        }
        .btn:hover {
            background-color: #2a602d;
        }
    </style>
</head>
<body>
<h1>Quản Lý Nhân Viên</h1>
<br>
<a href="<%=request.getContextPath()%>/insert" class="btn">Thêm Nhân Viên</a>
<h3>Danh Sách Sách</h3>
<table>
    <thead>
    <tr>
        <th>Mã Sách</th>
        <th>Tên Sách</th>
        <th>Danh mục</th>
        <th>Giá sách</th>
        <th>Lựa chọn</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="sach" items="${listSach}">
        <tr>
            <td><c:out value="${sach.maSach}"/></td>
            <td><c:out value="${sach.tenSach}"/></td>
            <td><c:out value="${sach.danhMuc}"/></td>
            <td><c:out value="${sach.giaSach}"/></td>

            <td>
                <a href="<%=request.getContextPath()%>/edit?maSach=${sach.maSach}">Sửa</a>
                <a href="<%=request.getContextPath()%>/delete?maSach=${sach.maSach}">Xoá</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>