<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
<head>
    <title>Kết quả kiểm tra tam giác</title>
</head>
<body>
<h2>Kết quả kiểm tra tam giác</h2>
Cạnh 1: <%= request.getParameter("a") %><br>
Cạnh 2: <%= request.getParameter("b") %><br>
Cạnh 3: <%= request.getParameter("c") %><br>
<%!
    String kiemTraTamGiac(double a, double b, double c) {
        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            if (a == b && a == c) {
                return "Tam giac đều";
            }
            if (a == b || a == c || b == c) {
                return "Tam giac cân";
            }
            if ((a * a + b * b) == c * c
                    || (a * a + c * c) == b * b
                    || (c * c + b * b) == a * a) {
                return "Tam giác vuông";
            }
            return "Tam giác";
        }
        return "Không phải tam giác";
    }


%>
<%
    try {
        double a = Double.parseDouble(request.getParameter("a"));
        double b = Double.parseDouble(request.getParameter("b"));
        double c = Double.parseDouble(request.getParameter("c"));
        out.println(kiemTraTamGiac(a, b, c));
    } catch (NumberFormatException e) {
        out.println("Không phải định dạng số");
    }

%>
</body>
</html>
