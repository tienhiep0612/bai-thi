<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>LIST SACH</title>
    <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }</style>
</head>
<body>
<h1>Quản Lý GV</h1>
<br>
<a href="<%=request.getContextPath()%>/insert" class="btn btn-success">Thêm Sách Mới</a>
<h3>Danh Sách GV </h3>
<table>
    <thead>
      <tr>
          <th>Mã GV</th>
          <th>Tên GV</th>
          <th>Tên bộ môn </th>
          <th>Hệ số lương </th>
          <th>Chức năng</th>
      </tr>
    </thead>
    <tbody>
        <c:forEach var="gv" items="${listgv}">
            <tr>
                <td><c:out value="${gv.maGiangVien}"/></td>
                <td><c:out value="${gv.hoTenGiangVien}"/></td>
                <td><c:out value="${gv.tenBoMon}"/></td>
                <td><c:out value="${gv.heSoLuong}"/></td>
                <td>
                    <a href="<%=request.getContextPath()%>/edit?maGiangVien=${gv.maGiangVien}">Sửa</a>
                    <a href="<%=request.getContextPath()%>/delete?maGiangVien=${gv.maGiangVien}">Xoá</a>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>


</body>
</html>
