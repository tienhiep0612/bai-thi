package model;

public class GiangVien {
	private Integer maGiangVien;
	private String hoTenGiangVien;
	private String tenBoMon;
	private Float heSoLuong;
	public GiangVien(Integer maGiangVien, String hoTenGiangVien, String tenBoMon, Float heSoLuong) {
		this.maGiangVien = maGiangVien;
		this.hoTenGiangVien = hoTenGiangVien;
		this.tenBoMon = tenBoMon;
		this.heSoLuong = heSoLuong;
	}
	
	public GiangVien(String hoTenGiangVien, String tenBoMon, Float heSoLuong) {
		this.hoTenGiangVien = hoTenGiangVien;
		this.tenBoMon = tenBoMon;
		this.heSoLuong = heSoLuong;
	}

	public GiangVien() {
		
	}
	public Integer getMaGiangVien() {
		return maGiangVien;
	}
	public void setMaGiangVien(Integer maGiangVien) {
		this.maGiangVien = maGiangVien;
	}
	public String getHoTenGiangVien() {
		return hoTenGiangVien;
	}
	public void setHoTenGiangVien(String hoTenGiangVien) {
		this.hoTenGiangVien = hoTenGiangVien;
	}
	public String getTenBoMon() {
		return tenBoMon;
	}
	public void setTenBoMon(String tenBoMon) {
		this.tenBoMon = tenBoMon;
	}
	public Float getHeSoLuong() {
		return heSoLuong;
	}
	public void setHeSoLuong(Float heSoLuong) {
		this.heSoLuong = heSoLuong;
	}
	
	
}
