package connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConnectionDB {
	private static final String DB_URL = "jdbc:mysql://localhost:3306/QL_giangvien";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public static Connection openConnection(){
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
    public static void closeConnection(Connection connection,
                                       PreparedStatement preparedStatement,
                                       ResultSet resultSet){
        try{
            if (connection != null){
                connection.close();
            }
            if (preparedStatement != null){
                preparedStatement.close();
            }
            if (resultSet != null)
            {
                resultSet.close();}
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
