package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import connect.ConnectionDB;
import model.GiangVien;

public class GiangVienService {
	private static final String SELECT_ALL_LIST_GV = "select * from t_giangvien;";
    private static final String INSERT_GV = "insert into t_giangvien(hoten,tenbomon,hesoluong) values ( ? , ? , ?)";
    private static final String DELETE_GV = "delete from t_giangvien where magv = ?;";
    private static final String UPDATE_GV = "update t_giangvien set hoten = ? , tenbomon = ?, hesoluong = ? where magv = ?";
    private static final String SELECT_GV_BY_ID = "select * from t_giangvien where magv = ?";
    public List<GiangVien> layDanhSachGV(){
        List<GiangVien> giangVienList = new ArrayList<>();
    try {
        Connection connection = ConnectionDB.openConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_LIST_GV);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            GiangVien gv = new GiangVien();
            gv.setMaGiangVien(resultSet.getInt("magv"));
            gv.setHoTenGiangVien(resultSet.getString("hoten"));
            gv.setTenBoMon(resultSet.getString("tenbomon"));
            gv.setHeSoLuong(resultSet.getFloat("hesoluong"));
            giangVienList.add(gv);
        }
        ConnectionDB.closeConnection(connection,preparedStatement,resultSet);
    }catch (SQLException e ){
        e.printStackTrace();
    }
        return giangVienList;
    }
    public Boolean addGV(GiangVien gv){
        Connection connection = null;
        try{
            connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_GV);
            preparedStatement.setString(1, gv.getHoTenGiangVien());
            preparedStatement.setString(2,gv.getTenBoMon());
            preparedStatement.setFloat(3,gv.getHeSoLuong());
            preparedStatement.executeUpdate();
            return true;
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }
    public Boolean deleteGV(Integer id){
        Connection connection = null;
        try {
            connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_GV);
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }
    
    public GiangVien getGVByID(Integer id){
    	GiangVien gv = new GiangVien();
        try{
            Connection connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_GV_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                gv.setMaGiangVien(resultSet.getInt("magv"));
                gv.setHoTenGiangVien(resultSet.getString("hoten"));
                gv.setTenBoMon(resultSet.getString("tenbomon"));
                gv.setHeSoLuong(resultSet.getFloat("hesoluong"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return gv;
    }
    
    
    public void updateGV(GiangVien gv){
        try {
            Connection connection = ConnectionDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_GV);
            preparedStatement.setString(1,gv.getHoTenGiangVien());
            preparedStatement.setString(2,gv.getTenBoMon());
            preparedStatement.setFloat(3,gv.getHeSoLuong());
            preparedStatement.setInt(4,gv.getMaGiangVien());
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

}
