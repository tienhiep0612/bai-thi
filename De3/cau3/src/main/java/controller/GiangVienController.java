package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.GiangVien;
import service.GiangVienService;

@WebServlet("/")

public class GiangVienController extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private GiangVienService giangVienService;
    public void init(){
    	giangVienService = new GiangVienService();
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rqParam = req.getServletPath();
        switch (rqParam){
            case "/insert":
            	showFormAddGV(req, resp);
                break;
            case "/add":
            	addGV(req,resp);
                break;
            case "/delete":
            	deleteGV(req, resp);
                break;
            case "/edit":
                showFormUpdateGV(req, resp);
                break;
            case "/update":
                updateGV(req, resp);
                break;
            default:
                showListGV(req,resp);
                break;
        }
    }
    private void showListGV(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<GiangVien> gvList = giangVienService.layDanhSachGV();
        req.setAttribute("listgv",gvList);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("danhsach.jsp");
        requestDispatcher.forward(req,resp);
    }
    private void showFormAddGV(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("them.jsp");
        requestDispatcher.forward(req,resp);
    }
    private void addGV(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("hoTenGiangVien");
        String mon = req.getParameter("tenBoMon");
        Float luong = Float.parseFloat(req.getParameter("luong"));
        GiangVien gv = new GiangVien(name,mon,luong);
        giangVienService.addGV(gv);
        resp.sendRedirect("ds");
    }
    private void deleteGV(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maGiangVien"));
        giangVienService.deleteGV(id);
        resp.sendRedirect("ds");
    }
    private void showFormUpdateGV(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("maGiangVien"));
        GiangVien gv = giangVienService.getGVByID(id);
        if (gv != null){
            req.setAttribute("maGiangVien",id);
            req.setAttribute("gv",gv);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("capnhat.jsp");
            requestDispatcher.forward(req,resp);
        }
    }
    private void updateGV(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maGiangVien"));
        String name = req.getParameter("hoTenGiangVien");
        String theloai = req.getParameter("tenBoMon");
        Float price = Float.parseFloat(req.getParameter("luong"));
        GiangVien gv = new GiangVien(id,name,theloai,price);
        giangVienService.updateGV(gv);
        resp.sendRedirect("list");
    }
}
