<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Nhập cạnh tam giác</title>
</head>
<body>
<h2>Nhập cạnh tam giác</h2>
<form action="result.jsp" method="post">
    <label for="side1">Cạnh 1:</label>
    <input type="text" name="side1" required><br>

    <label for="side2">Cạnh 2:</label>
    <input type="text" name="side2" required><br>

    <label for="side3">Cạnh 3:</label>
    <input type="text" name="side3" required><br>

    <input type="submit" value="Kiểm tra tam giác">
</form>
</body>
</html>
