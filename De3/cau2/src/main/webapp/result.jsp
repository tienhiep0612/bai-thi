<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
<head>
    <title>Kết quả kiểm tra tam giác</title>
</head>
<body>
<h2>Kết quả kiểm tra tam giác</h2>
<%
    String side1Str = request.getParameter("side1");
    String side2Str = request.getParameter("side2");
    String side3Str = request.getParameter("side3");
    double side1 = Double.parseDouble(side1Str);
    double side2 = Double.parseDouble(side2Str);
    double side3 = Double.parseDouble(side3Str);
    boolean isTriangle = (side1 + side2 > side3) && (side1 + side3 > side2) && (side2 + side3 > side1);
    double area = 0.0;
    if (isTriangle) {
        double s = (side1 + side2 + side3) / 2;
        area = Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }
%>
<p>
    Cạnh 1: <%= side1 %><br>
    Cạnh 2: <%= side2 %><br>
    Cạnh 3: <%= side3 %><br>
</p>
<p>
    <%
        if (isTriangle) {
            out.println("Ba cạnh lập thành một tam giác.");
            out.println("Diện tích tam giác: " + new DecimalFormat("#.##").format(area));
        } else {
            out.println("Ba cạnh không lập thành một tam giác.");
        }
    %>
</p>
</body>
</html>
