<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h2>Giải phương trình bậc 2</h2>

<form action="" method="post">
    Nhập giá trị a: <input type="text" name="a"><br>
    Nhập giá trị b: <input type="text" name="b"><br>
    Nhập giá trị c: <input type="text" name="c"><br>
    <input type="submit" value="Kết quả">
</form>

<%-- Xử lý khi nhấn nút "Kết quả" --%>
<%@ page import="java.util.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="static java.lang.System.out" %>
<%!
    double calculateDelta(double a, double b, double c) {
        return b * b - 4 * a * c;
    }
    String solveEquation(double a, double b, double c) {
        if (a == 0) {
            if (b == 0) {
                if (c == 0) {
                    return "Vô số nghiệm";
                } else {
                    return "Vô nghiệm";
                }
            }else {
                double kq = ((-c)/b);
                return "Phương trình có một nghiệm: x = " + kq;
            }
        } else {
            double delta = calculateDelta(a, b, c);
            if (delta < 0) {
                return "Phương trình vô nghiệm";
            } else if (delta == 0) {
                double root = -b / (2 * a);
                return "Phương trình có nghiệm kép x = " + root;
            } else {
                double root1 = (-b + Math.sqrt(delta)) / (2 * a);
                double root2 = (-b - Math.sqrt(delta)) / (2 * a);
                return "Phương trình có hai nghiệm phân biệt: x1 = " + root1 + ", x2 = " + root2;
            }
        }
    }
%>

<%-- Xử lý khi submit form --%>
<%
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        try {
            double a = Double.parseDouble(request.getParameter("a"));
            double b = Double.parseDouble(request.getParameter("b"));
            double c = Double.parseDouble(request.getParameter("c"));

            String result = solveEquation(a, b, c);
            out.println("<p>Kết quả: " + result + "</p>");
        } catch (NumberFormatException e) {
            out.println("<p>Vui lòng nhập đúng định dạng số.</p>");
        }
    }
%>
</body>
</html>