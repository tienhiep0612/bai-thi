<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>CẬP NHẬT SÁCH MỚI</title>
    <style>
        /* Add your CSS here */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 5px;
            text-align: center;
            cursor: pointer;
            text-decoration:none;
        }
        .btn:hover {
            background-color: #45a049;
        }
        form {
            width: 100%;
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1);
            background-color: #fff;
        }
        form label {
            display: block;
            margin-bottom: 5px;
        }
        form input[type="text"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ddd;
            border-radius: 5px;
        }
        form button[type="submit"] {
            display: block;
            width: 100%;
            padding: 10px;
            border: none;
            border-radius: 5px;
            color: #fff;
            background-color: #333;
            cursor: pointer;
        }
        form button[type="submit"]:hover {
            background-color: #444;
        }
    </style>
</head>
<body>
<a href="<%=request.getContextPath()%>/list" class="btn btn-success">SHOW DANH SÁCH NHÂN VIÊN HIỆN CÓ</a>
<br>
<form action="update" method="post">
    <input type="hidden" name="maNV" value="${maNV}">

    <label>Họ Nhân Viên</label>
    <input type="text" name="hoNV" value="${nhanVien.hoNV}" required >
    <br>
    <label>Tên Nhân Viên</label>
    <input type="text" name="tenNV" value="${nhanVien.tenNV}" required >
    <br>
    <label>Giới Tính</label>
    <input type="text" name="gioiTinh" value="${nhanVien.gioiTinh}" required >
    <br>
    <label>Ngày Sinh</label>
    <input type="text" name="ngaySinh" value="${nhanVien.ngaySinh}" required >
    <br>
    <label>Địa chỉ</label>
    <input type="text" name="diaChi" value="${nhanVien.diaChi}" required >
    <br>

    <button type="submit">Cập Nhật</button>
</form>
</body>
</html>