<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>LIST NV</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
        }
        h1 {
            color: #333;
            text-align: center;
            padding: 20px 0;
        }
        table {
            width: 100%;
            margin: 20px 0;
            border-collapse: collapse;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ddd;
            text-align: left;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        a {
            color: #333;
            text-decoration: none;
            margin: 0 10px;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 5px;
            text-align: center;
            cursor: pointer;
        }
        .btn:hover {
            background-color: #2a602d;
        }
    </style>
</head>
<body>
<h1>Quản Lý Nhân Viên</h1>
<br>
<a href="<%=request.getContextPath()%>/insert" class="btn">Thêm Nhân Viên</a>
<h3>Danh Sách Nhân Viên</h3>
<table>
    <thead>
    <tr>
        <th>Mã NV</th>
        <th>Họ NV</th>
        <th>Tên NV</th>
        <th>Giới tính</th>
        <th>Ngày sinh</th>
        <th>Địa chỉ</th>
        <th>Lựa chọn</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="nhanVien" items="${listNhanVien}">
        <tr>
            <td><c:out value="${nhanVien.maNV}"/></td>
            <td><c:out value="${nhanVien.hoNV}"/></td>
            <td><c:out value="${nhanVien.tenNV}"/></td>
            <td><c:out value="${nhanVien.gioiTinh}"/></td>
            <td><c:out value="${nhanVien.ngaySinh}"/></td>
            <td><c:out value="${nhanVien.diaChi}"/></td>

            <td>
                <a href="<%=request.getContextPath()%>/edit?maNV=${nhanVien.maNV}">Sửa</a>
                <a href="<%=request.getContextPath()%>/delete?maNV=${nhanVien.maNV}">Xoá</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>