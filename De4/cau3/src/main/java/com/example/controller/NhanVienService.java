package com.example.controller;

import com.example.model.NhanVien;
import com.example.connection.NhanVienDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NhanVienService {
    static Connection connection;
    private static final String SELECT_ALL_FROM_NV = "select * from nhanvien;";
    private static final String INSERT_NV = "insert into nhanvien(ho_nv,ten_nv,gioi_tinh,ngay_sinh,dia_chi) values (?,?,?,?,?);";
    private static final String DELETE_NV = "delete from nhanvien where ma_nv = ?;";
    private static final String UPDATE_NV = "update nhanvien set ho_nv = ?, ten_nv = ?, gioi_tinh = ?, ngay_sinh = ?, dia_chi = ? where ma_nv = ?;";
    private static final String SELECTION_NV_BY_IDNV = "select * from nhanvien where ma_nv = ?;";

    public Boolean themNV(NhanVien nhan_vien) {
        try {
            connection = NhanVienDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NV);
            preparedStatement.setString(1, nhan_vien.getHoNV());
            preparedStatement.setString(2, nhan_vien.getTenNV());
            preparedStatement.setString(3, nhan_vien.getGioiTinh());
            preparedStatement.setString(4, nhan_vien.getNgaySinh());
            preparedStatement.setString(5, nhan_vien.getDiaChi());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<NhanVien> danhsachNV() {
        List<NhanVien> NhanVienList = new ArrayList<>();
        try {
            connection = NhanVienDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM_NV);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                NhanVien nhan_vien = new NhanVien();
                nhan_vien.setMaNV(resultSet.getInt("ma_nv"));
                nhan_vien.setHoNV(resultSet.getString("ho_nv"));
                nhan_vien.setTenNV(resultSet.getString("ten_nv"));
                nhan_vien.setGioiTinh(resultSet.getString("gioi_tinh"));
                nhan_vien.setNgaySinh(resultSet.getString("ngay_sinh"));
                nhan_vien.setDiaChi(resultSet.getString("dia_chi"));
                NhanVienList.add(nhan_vien);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return NhanVienList;
    }

    public Boolean xoaNV(Integer id) {
        try {
            connection = NhanVienDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NV);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public NhanVien layIDNV(Integer id) {
        NhanVien nhan_vien = new NhanVien();
        try {
            connection = NhanVienDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECTION_NV_BY_IDNV);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                nhan_vien.setMaNV(resultSet.getInt("ma_nv"));
                nhan_vien.setHoNV(resultSet.getString("ho_nv"));
                nhan_vien.setTenNV(resultSet.getString("ten_nv"));
                nhan_vien.setGioiTinh(resultSet.getString("gioi_tinh"));
                nhan_vien.setNgaySinh(resultSet.getString("ngay_sinh"));
                nhan_vien.setDiaChi(resultSet.getString("dia_chi"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nhan_vien;
    }

    public void capnhatNV(NhanVien nhan_vien) {
        try {
            connection = NhanVienDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NV);
            preparedStatement.setString(1, nhan_vien.getHoNV());
            preparedStatement.setString(2, nhan_vien.getTenNV());
            preparedStatement.setString(3, nhan_vien.getGioiTinh());
            preparedStatement.setString(4, nhan_vien.getNgaySinh());
            preparedStatement.setString(5, nhan_vien.getDiaChi());
            preparedStatement.setInt(6, nhan_vien.getMaNV());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

