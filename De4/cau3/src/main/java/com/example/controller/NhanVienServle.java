package com.example.controller;

import com.example.model.NhanVien;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class NhanVienServle extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private NhanVienService nhanVienService;
    public void init(){
        nhanVienService = new NhanVienService();
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rqParam = req.getServletPath();
        switch (rqParam){
            case "/insert":
                showFormThemNhanVien(req, resp);
                break;
            case "/add":
                themNhanVien(req,resp);
                break;
            case "/delete":
                xoaNhanVien(req, resp);
                break;
            case "/edit":
                showFormUpdateNhanVien(req, resp);
                break;
            case "/update":
                capNhatNhanVien(req, resp);
                break;
            default:
                showListNhanVien(req,resp);
                break;
        }
    }
    private void showListNhanVien(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<NhanVien> nhanViens = nhanVienService.danhsachNV();
        req.setAttribute("listNhanVien" , nhanViens);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/list.jsp");
        dispatcher.forward(req,resp);
    }
    private void showFormThemNhanVien(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/them.jsp");
        requestDispatcher.forward(req,resp);
    }
    private void themNhanVien(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String hoNV =req.getParameter("hoNV");
        String tenNV = req.getParameter("tenNV");
        String gioiTinh = req.getParameter("gioiTinh");
        String ngaySinh = req.getParameter("ngaySinh");
        String diaChi = req.getParameter("diaChi");
        NhanVien NhanVien = new NhanVien(hoNV,tenNV,gioiTinh,ngaySinh,diaChi);
        nhanVienService.themNV(NhanVien);
        resp.sendRedirect("list");
    }
    private void xoaNhanVien(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maNV"));
        nhanVienService.xoaNV(id);
        resp.sendRedirect("list");
    }
    private void showFormUpdateNhanVien(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("maNV"));
        NhanVien NhanVien = nhanVienService.layIDNV(id);
        if (NhanVien != null){
            req.setAttribute("maNV",id);
            req.setAttribute("nhanVien",NhanVien);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/sua.jsp");
            requestDispatcher.forward(req,resp);
        }
    }
    private void capNhatNhanVien(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("maNV"));
        String hoNV =req.getParameter("hoNV");
        String tenNV = req.getParameter("tenNV");
        String gioiTinh = req.getParameter("gioiTinh");
        String ngaySinh = req.getParameter("ngaySinh");
        String diaChi = req.getParameter("diaChi");
        NhanVien NhanVien = new NhanVien(id,hoNV,tenNV,gioiTinh,ngaySinh,diaChi);
        nhanVienService.capnhatNV(NhanVien);
        resp.sendRedirect("list");
    }
}
