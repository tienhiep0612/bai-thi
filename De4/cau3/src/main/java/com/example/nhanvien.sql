create database nhanviendb;

create table nhanviendb.nhanvien
(
    ma_nv     int         not null auto_increment,
    ho_nv     varchar(50) not null,
    ten_nv    varchar(50) not null,
    gioi_tinh varchar(50) not null,
    ngay_sinh varchar(50) not null,
    dia_chi   varchar(50) not null,
    primary key (ma_nv)
);