package com.example.controller;

import com.example.model.HangHoa;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class BanHangServle extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private BanHangService banHangService;
    public void init(){
        banHangService = new BanHangService();
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rqParam = req.getServletPath();
        switch (rqParam){
            case "/insert":
                showFormThemHangHoa(req, resp);
                break;
            case "/add":
                themHangHoa(req,resp);
                break;
            case "/delete":
                xoaHangHoa(req, resp);
                break;
            case "/edit":
                showFormUpdateHangHoa(req, resp);
                break;
            case "/update":
                capNhatHangHoa(req, resp);
                break;
            default:
                showListHangHoa(req,resp);
                break;
        }
    }
    private void showListHangHoa(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<HangHoa> sinhVienList = banHangService.danhsachHH();
        req.setAttribute("listHangHoa" , sinhVienList);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/list.jsp");
        dispatcher.forward(req,resp);
    }
    private void showFormThemHangHoa(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/them.jsp");
        requestDispatcher.forward(req,resp);
    }
    private void themHangHoa(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String tenHH =req.getParameter("tenHH");
        String ngayNhap = req.getParameter("ngayNhap");
        String nhomHang = req.getParameter("nhomHang");
        String soLuong = req.getParameter("soLuong");
        HangHoa HangHoa = new HangHoa(tenHH,ngayNhap,nhomHang,soLuong);
        banHangService.themHH(HangHoa);
        resp.sendRedirect("list");
    }
    private void xoaHangHoa(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("id"));
        banHangService.xoaHH(id);
        resp.sendRedirect("list");
    }
    private void showFormUpdateHangHoa(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.parseInt(req.getParameter("id"));
        HangHoa HangHoa = banHangService.layIDHH(id);
        if (HangHoa != null){
            req.setAttribute("id",id);
            req.setAttribute("hangHoa", HangHoa);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/view/sua.jsp");
            requestDispatcher.forward(req,resp);
        }
    }
    private void capNhatHangHoa(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer id = Integer.parseInt(req.getParameter("id"));
        String tenHH =req.getParameter("tenHH");
        String ngayNhap = req.getParameter("ngayNhap");
        String nhomHang = req.getParameter("nhomHang");
        String soLuong = req.getParameter("soLuong");
        HangHoa HangHoa = new HangHoa(id,tenHH,ngayNhap,nhomHang,soLuong);
        banHangService.capnhatHH(HangHoa);
        resp.sendRedirect("list");
    }
}
