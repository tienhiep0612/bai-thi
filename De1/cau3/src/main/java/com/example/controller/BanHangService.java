package com.example.controller;

import com.example.model.HangHoa;
import com.example.connection.BanHangDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BanHangService {
    static Connection connection;
    private static final String SELECT_ALL_FROM_HH = "select * from hanghoa;";
    private static final String INSERT_HH = "insert into hanghoa(tenhang,ngaynhap,nhomhang,soluong) values (?,?,?,?);";
    private static final String DELETE_HH = "delete from hanghoa where id = ?;";
    private static final String UPDATE_HH = "update hanghoa set tenhang = ?, ngaynhap = ?, nhomhang = ?, soluong = ? where id = ?;";
    private static final String SELECTION_HH_BY_IDHH = "select * from hanghoa where id = ?;";

    public Boolean themHH(HangHoa hang_hoa) {
        try {
            connection = BanHangDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HH);
            preparedStatement.setString(1, hang_hoa.getTenHang());
            preparedStatement.setString(2, hang_hoa.getNgayNhap());
            preparedStatement.setString(3, hang_hoa.getNhomHang());
            preparedStatement.setString(4, hang_hoa.getSoLuong());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<HangHoa> danhsachHH() {
        List<HangHoa> hangHoaList = new ArrayList<>();
        try {
            connection = BanHangDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM_HH);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                HangHoa hang_hoa = new HangHoa();
                hang_hoa.setID(resultSet.getInt("id"));
                hang_hoa.setTenHang(resultSet.getString("tenhang"));
                hang_hoa.setNgayNhap(resultSet.getString("ngaynhap"));
                hang_hoa.setNhomHang(resultSet.getString("nhomhang"));
                hang_hoa.setSoLuong(resultSet.getString("soluong"));
                hangHoaList.add(hang_hoa);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hangHoaList;
    }

    public Boolean xoaHH(Integer id) {
        try {
            connection = BanHangDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_HH);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public HangHoa layIDHH(Integer id) {
        HangHoa hang_hoa = new HangHoa();
        try {
            connection = BanHangDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECTION_HH_BY_IDHH);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                hang_hoa.setID(resultSet.getInt("id"));
                hang_hoa.setTenHang(resultSet.getString("tenhang"));
                hang_hoa.setNgayNhap(resultSet.getString("ngaynhap"));
                hang_hoa.setNhomHang(resultSet.getString("nhomhang"));
                hang_hoa.setSoLuong(resultSet.getString("soluong"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hang_hoa;
    }

    public void capnhatHH(HangHoa hang_hoa) {
        try {
            connection = BanHangDB.openConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HH);
            preparedStatement.setString(1, hang_hoa.getTenHang());
            preparedStatement.setString(2, hang_hoa.getNgayNhap());
            preparedStatement.setString(3, hang_hoa.getNhomHang());
            preparedStatement.setString(4, hang_hoa.getSoLuong());
            preparedStatement.setInt(5, hang_hoa.getID());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

