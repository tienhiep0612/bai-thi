package com.example.model;

public class HangHoa {
    private Integer ID;
    private String tenHang;
    private String ngayNhap;
    private String nhomHang;
    private String soLuong;

    public HangHoa() {
    }

    public HangHoa(String tenHang, String ngayNhap, String nhomHang, String soLuong) {
        this.tenHang = tenHang;
        this.ngayNhap = ngayNhap;
        this.nhomHang = nhomHang;
        this.soLuong = soLuong;
    }

    public HangHoa(Integer ID, String tenHang, String ngayNhap, String nhomHang, String soLuong) {
        this.ID = ID;
        this.tenHang = tenHang;
        this.ngayNhap = ngayNhap;
        this.nhomHang = nhomHang;
        this.soLuong = soLuong;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getTenHang() {
        return tenHang;
    }

    public void setTenHang(String tenHang) {
        this.tenHang = tenHang;
    }

    public String getNgayNhap() {
        return ngayNhap;
    }

    public void setNgayNhap(String ngayNhap) {
        this.ngayNhap = ngayNhap;
    }

    public String getNhomHang() {
        return nhomHang;
    }

    public void setNhomHang(String nhomHang) {
        this.nhomHang = nhomHang;
    }

    public String getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(String soLuong) {
        this.soLuong = soLuong;
    }
}