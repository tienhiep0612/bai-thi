create database banhangdb;

create table banhangdb.hanghoa
(
    id       int         not null auto_increment,
    tenhang  varchar(50) not null,
    ngaynhap varchar(50) not null,
    nhomhang varchar(50) not null,
    soluong  varchar(50) not null,
    primary key (id)
);