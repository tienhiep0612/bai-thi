<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title>LIST HH</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
        }
        h1 {
            color: #333;
            text-align: center;
            padding: 20px 0;
        }
        table {
            width: 100%;
            margin: 20px 0;
            border-collapse: collapse;
        }
        th, td {
            padding: 10px;
            border: 1px solid #ddd;
            text-align: left;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        a {
            color: #333;
            text-decoration: none;
            margin: 0 10px;
        }
        .btn {
            display: inline-block;
            padding: 10px 20px;
            color: #fff;
            background-color: #4CAF50;
            border: none;
            border-radius: 5px;
            text-align: center;
            cursor: pointer;
        }
        .btn:hover {
            background-color: #2a602d;
        }
    </style>
</head>
<body>
<h1>Quản Lý Bán Hàng</h1>
<br>
<a href="<%=request.getContextPath()%>/insert" class="btn">Thêm Hàng Hóa</a>
<h3>Danh Sách Hàng Hóa</h3>
<table>
    <thead>
    <tr>
        <th>Mã HH</th>
        <th>Tên HH</th>
        <th>Ngày nhập</th>
        <th>Nhóm hàng</th>
        <th>Số lượng</th>
        <th>Lựa chọn</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="hangHoa" items="${listHangHoa}">
        <tr>
            <td><c:out value="${hangHoa.ID}"/></td>
            <td><c:out value="${hangHoa.tenHang}"/></td>
            <td><c:out value="${hangHoa.ngayNhap}"/></td>
            <td><c:out value="${hangHoa.nhomHang}"/></td>
            <td><c:out value="${hangHoa.soLuong}"/></td>

            <td>
                <a href="<%=request.getContextPath()%>/edit?id=${hangHoa.ID}">Sửa</a>
                <a href="<%=request.getContextPath()%>/delete?id=${hangHoa.ID}">Xoá</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>