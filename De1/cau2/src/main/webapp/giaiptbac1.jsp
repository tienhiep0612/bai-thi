<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<form action="" method="post">
    Nhap gia tri a: <input type="text" name="a">
    <br>
    Nhap gia tri b: <input type="text" name="b">
    <br>
    <input type="submit" value="Kết quả">
</form>

<%
    if("POST".equalsIgnoreCase(request.getMethod())){
        try{
            Double a = Double.parseDouble(request.getParameter("a"));
            Double b = Double.parseDouble(request.getParameter("b"));
            if(a == 0){
                if(b == 0) out.println("Phương trình vô số nghiệm");
                else out.println("Phương trình vô nghiệm");
            }else{
                Double x = -b/a;
                out.println("Phương trình có nghiệm: x = " + x );
            }
        }catch (NumberFormatException e ){
            out.println("<p>Vui lòng nhập đúng định dạng số.</p>");
        }
    }


%>

</body>
</html>